/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.manager;

import com.sigpro.dao.impl.UnidadeImpl;
import com.sigpro.entidades.Unidade;
import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 * Created on : 06/01/2015, 12:15:28
 *
 * @author Elis Oliveira
 */
@FacesConverter(value = "unidadeConverter")
public class UnidadeConverter implements Converter {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Query q = manager.createQuery("SELECT u FROM Unidade u WHERE u.nome = :nome").setParameter("nome", value);
        return q.getSingleResult();
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        System.out.println("as string");
        if (o != null) {
            return String.valueOf(((Unidade) o).getId());
        } else {
            return null;
        }
    }

}
