/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.EncaminhamentoImpl;
import com.sigpro.dao.impl.UnidadeImpl;
import com.sigpro.dao.interfaces.EncaminhaDao;
import com.sigpro.dao.interfaces.UnidadeDao;
import com.sigpro.entidades.Unidade;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

/**
 * Created on : 05/12/2014, 10:48:08
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "encaminhaMB")
@RequestScoped
public class EncaminhaController {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    @Resource
    private UserTransaction transaction;

    private String valorDigitadoNoCampo;
    private UnidadeDao unidadeDao;
    private EncaminhaDao encaminhaDao;
    private Unidade unidade;
    private List<Unidade> listaUnidades;

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public List<Unidade> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<Unidade> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

//     public List<Unidade> completeUnidade(String valorDigitado) {
//       
//        List<Unidade> allUnidades = manager.createNamedQuery(Unidade.BUSCAR_UNIDADE_PELO_NOME).getResultList();
//        List<Unidade> unidadesFiltradas = new ArrayList<>();
//        for (Unidade u : allUnidades) {
//            if (u.getNome().toLowerCase().startsWith(valorDigitado)) {
//                unidadesFiltradas.add(u);
//            }
//        }
//        return unidadesFiltradas;
//    }
    
    public List<Unidade> completeUnidade(String valorDigitado) {
        List<Unidade> unidades = unidadeDao.listarTodasUnidades();
        List<Unidade> unidadesFiltradas = new ArrayList<>();
        for (Unidade u : unidades) {
            if (u.getNome().toLowerCase().startsWith(valorDigitado)) {
                unidadesFiltradas.add(u);
            }
        }
        return unidadesFiltradas;
    }  

    @PostConstruct
    public void onConstruct() {
        encaminhaDao = new EncaminhamentoImpl(manager, transaction);
        unidadeDao = new UnidadeImpl(manager, transaction);
    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        encaminhaDao = null;
        unidadeDao = null;
    }

}
