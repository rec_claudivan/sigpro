/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created on : 12/11/2014, 16:27:39
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "autocompleteMB")
@RequestScoped
public class TesteConexaoMB {

    @PersistenceContext(unitName = "SIGPRO-PU")
    private EntityManager manager;

    private String nameOfUser;

    public String getNameOfUser() {
        return nameOfUser;
    }

    public void setNameOfUser(String nameOfUser) {
        this.nameOfUser = nameOfUser;
    }

    public List<String> completeText(String rairai) {
        List<Usuario> users = getUsers(rairai);
        List<String> nomes = new ArrayList<>();
        System.out.println(users.size());
        for (Usuario u: users){
            nomes.add(u.getNome());
        }
        
        return nomes;

    }

    private List<Usuario> getUsers(String rairai) {

        String query = "select u from Usuario u where u.nome like '%" + rairai + "%' order by u.nome";
        
        System.out.println(query);
        
//        return manager.createQuery(query).getResultList();
        return new ArrayList<>();
    }

}
