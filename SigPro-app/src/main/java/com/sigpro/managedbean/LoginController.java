/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.interfaces.LoginDao;
import com.sigpro.entidades.Servidor;
import com.sigpro.entidades.Usuario;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

/**
 * Created on : 14/12/2014, 23:50:08
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "loginMB")
@RequestScoped
public class LoginController {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    @Resource
    private UserTransaction transaction;
    private LoginDao loginDao;

    private String login;
    private String senha;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String doLogin() {
        Usuario usuario = loginDao.find(login, senha);
        if (usuario == null) {
            System.out.println("usuario nulo");
            return "login.xhtml";

        } else {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute("usuarioLogado", usuario);
            if (usuario instanceof Servidor) {
                return "index.xhtml";
            } else {
                return null;
            }
        }
    }

    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.removeAttribute("usuarioLogado");
        session.invalidate();
        return "index.jsf";
    }

}
