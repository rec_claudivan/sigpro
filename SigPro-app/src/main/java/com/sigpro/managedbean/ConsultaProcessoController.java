/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.ProcessoImpl;
import com.sigpro.dao.interfaces.ProcessoDao;
import com.sigpro.entidades.Processo;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created on : 14/12/2014, 18:17:05
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "consultaMB")
@RequestScoped
public class ConsultaProcessoController {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    private String filtro;
    private String palavraChave;
    private ProcessoDao dao;
    private DataModel<Processo> dataModelConsultaProcesso;

    public void buscarProcesso() {

        List<Processo> processos;

        switch (filtro) {
            case "assunto":
                processos = dao.filterProcess(Processo.BUSCAR_POR_ASSUNTO, "%" + palavraChave.toUpperCase() + "%");
                break;
            case "unidade":
                processos = dao.filterProcess(Processo.BUSCAR_POR_UNIDADE, "%" + palavraChave.toUpperCase() + "%");
                break;
            case "interessado":
                processos = dao.filterProcess(Processo.BUSCAR_POR_NOME_INTERESSADO, "%" + palavraChave.toUpperCase() + "%");
                break;
            default:
                processos = dao.filterProcess(Processo.BUSCAR_POR_NUM_PROCESSO, "%" + palavraChave + "%");
                break;
        }

        dataModelConsultaProcesso = new ListDataModel<>(processos);

    }

    public DataModel<Processo> getDataModelConsultaProcesso() {
        return dataModelConsultaProcesso;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public String getPalavraChave() {
        return palavraChave;
    }

    public void setPalavraChave(String palavraChave) {
        this.palavraChave = palavraChave;
    }

    @PostConstruct
    public void onConstruct() {
        dao = new ProcessoImpl(manager, null);
    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        dao = null;
    }

}
