/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.EncaminhamentoImpl;
import com.sigpro.dao.impl.ProcessoImpl;
import com.sigpro.dao.interfaces.EncaminhaDao;
import com.sigpro.dao.interfaces.ProcessoDao;
import com.sigpro.entidades.Encaminhamento;
import com.sigpro.entidades.Processo;
import com.sigpro.entidades.TipoStatusEncaminhamento;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

/**
 * Created on : 02/12/2014, 16:41:10
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "entradaMB")
@RequestScoped
public class EntradaController {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    @Resource
    private UserTransaction transaction;
    private EncaminhaDao dao;
    private Encaminhamento encaminhamento = new Encaminhamento();
    private DataModel<Encaminhamento> dataModelEntrada;

    public DataModel<Encaminhamento> getDataModelEntrada() {
        if (dataModelEntrada == null) {
            List<Encaminhamento> listaEntrada = dao.findAllEntrada();
            dataModelEntrada = new ListDataModel<>(listaEntrada);
        }
        return dataModelEntrada;
    }

    public void setDataModelEntrada(DataModel<Encaminhamento> dataModelEntrada) {
        this.dataModelEntrada = dataModelEntrada;
    }

    public void receberProcesso() {

        try {

            Encaminhamento e = dataModelEntrada.getRowData();
            dao.find(e.getId());

            e.setStatus(TipoStatusEncaminhamento.PENDENTE);
            dao.update(e);

            System.out.println("STATUS ATUALIZADO!");

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "Sucesso!",
                    "O processo agora está na caixa de processos pendentes!"));

            dataModelEntrada = null;

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Erro ao tentar receber!",
                    "Detalhes do Erro: " + e.getMessage()));
        }

    }

    @PostConstruct
    public void onConstruct() {
        dao = new EncaminhamentoImpl(manager, transaction);
    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        dao = null;
    }

}
