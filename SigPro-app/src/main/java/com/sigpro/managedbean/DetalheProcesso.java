/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.ProcessoImpl;
import com.sigpro.dao.interfaces.ProcessoDao;
import com.sigpro.entidades.Processo;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

/**
 *
 * @author elisiany
 */
@ManagedBean(name = "detalheMB")
@RequestScoped
public class DetalheProcesso {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    @Resource
    private UserTransaction transaction;
    @ManagedProperty(value = "#{param.codigoProcesso}")
    private String codigoProcesso;
    private Processo processo;
    private ProcessoDao processoDao;

    public DetalheProcesso() {
    }

    public String getCodigoProcesso() {
        return codigoProcesso;
    }

    public void setCodigoProcesso(String codigoProcesso) {
        this.codigoProcesso = codigoProcesso;
    }

    public Processo getProcesso() {

        System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("codigoProcesso"));
        processo = processoDao.buscaProcessoPeloCodigo(Processo.BUSCAR_POR_UM_PROCESSO_PELO_CODIGO, codigoProcesso);

        System.out.println(processo.getAssunto());

        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    @PostConstruct
    public void onConstruct() {
        processoDao = new ProcessoImpl(manager, transaction);
    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        processoDao = null;
    }

}
