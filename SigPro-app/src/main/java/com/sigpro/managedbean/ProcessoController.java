/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.EncaminhamentoImpl;
import com.sigpro.dao.impl.ProcessoImpl;
import com.sigpro.dao.interfaces.DAO;
import com.sigpro.dao.interfaces.EncaminhaDao;
import com.sigpro.dao.interfaces.ProcessoDao;
import com.sigpro.entidades.Anexo;
import com.sigpro.entidades.Encaminhamento;
import com.sigpro.entidades.Processo;
import com.sigpro.entidades.TipoDecisao;
import com.sigpro.entidades.TipoStatusEncaminhamento;
import com.sigpro.entidades.TipoStatusProcesso;
import com.sigpro.entidades.Unidade;
import com.sigpro.entidades.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.primefaces.event.FileUploadEvent;

/**
 * Created on : 02/12/2014, 14:19:47
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "processoMB")
@ViewScoped
public class ProcessoController implements Serializable {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;

    @Resource
    private UserTransaction transaction;

    private ProcessoDao processoDAO;
    private Processo processo = new Processo();
    private EncaminhaDao encaminhaDAO;
    private Encaminhamento encaminhamento;
    private DAO dao;
    private Unidade unidadeDestinoPadrao;
    private Usuario user;
    private List<Anexo> listaDeAnexos;
    private List<Encaminhamento> listaDeEncaminhamentos;
    private DataModel<Anexo> dataModelAnexos;
    private DataModel<Processo> dataModelProcessos;

    private String codigoDoProcesso;

    private final String DIR_DOCUMENTOS = FacesContext.getCurrentInstance().
            getExternalContext().getRealPath("") + "/documentos/";

    public ProcessoController() {
        unidadeDestinoPadrao = new Unidade(1L);
        user = new Usuario("0000");
        encaminhamento = new Encaminhamento();
        listaDeAnexos = new ArrayList<>();
    }

    public String getCodigoDoProcesso() {
        return codigoDoProcesso;
    }

    public void setCodigoDoProcesso(String codigoDoProcesso) {
        this.codigoDoProcesso = codigoDoProcesso;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public DataModel<Anexo> getDataModelAnexos() {
        return dataModelAnexos;
    }

    public DataModel<Processo> getDataModelProcessos() {

        ProcessoDao dao = new ProcessoImpl();

        List<Processo> processos = dao.findAll();

        dataModelProcessos = new ListDataModel<>(processos);
        return dataModelProcessos;
    }

    public void salvaProcesso() {
        Date hoje = new Date();
        SimpleDateFormat formatadorDeData = new SimpleDateFormat("yyyy-MM");

        StringBuilder codProcessoBuilder = new StringBuilder("23324.");
        codProcessoBuilder.append(System.currentTimeMillis());
        codProcessoBuilder.append(".");
        codProcessoBuilder.append(formatadorDeData.format(hoje));

        processo.setCodigo(codProcessoBuilder.toString());
        processo.setDataCadastro(hoje);
        processo.setStatusProcesso(TipoStatusProcesso.ATIVO);
        processo.setDecisao(TipoDecisao.PENDENTE);
        processo.setUnidadeDestino(unidadeDestinoPadrao);
        processo.setSolicitante(user);
        processo.setStatusEncaminhamento(TipoStatusEncaminhamento.ENTRADA);

        encaminhamento.setAssunto(processo.getAssunto());
        encaminhamento.setDataEncaminhamento(hoje);
        encaminhamento.setStatus(TipoStatusEncaminhamento.ENTRADA);
        encaminhamento.setUnidadeDestino(unidadeDestinoPadrao);

        try {

            System.out.println("Entrou no try!");

            processo.setAnexos(listaDeAnexos);
            System.out.println("setou lista de anexos no processo!");

            encaminhamento.setAnexos(listaDeAnexos);
            System.out.println("setou lista de anexos no encaminhamento!");

            listaDeEncaminhamentos = new ArrayList<>();

            listaDeEncaminhamentos.add(encaminhamento);
            System.out.println("add encaminhamento na lista de encaminhamento");

            processoDAO.save(processo);
            System.out.println("salvou o processo! ebaaaaa");

            for (Anexo anexo : listaDeAnexos) {
                anexo.setProcesso(processo);
                anexo.setEncaminhamento(encaminhamento);
            }
            System.out.println("passou pelo for, adicionando os anexos ao processo e encaminhamento");

            encaminhamento.setProcesso(processo);
            System.out.println("add processo em encaminhamento");

            processo.getEncaminhamentos().add(encaminhamento);
            processoDAO.update(processo);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "Sucesso!",
                    "O processo foi cadastrado com Sucesso! Aguarde o deferimento."));

            this.processo = new Processo();
            listaDeAnexos.clear();
            System.out.println("CLEAR DE ANEXOS!");
            listaDeEncaminhamentos.clear();
            System.out.println("CLEAR DE ENCAMINHAMENTO!");

//            this.processo = null;
            System.out.println("TERMINOU, VLW FLW!");

        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Erro ao tentar cadastrar!",
                    "Detalhes do Erro: " + ex.getMessage() + ". Tente novamente mais tarde."));
        }
    }

    public void uploadDocumento(FileUploadEvent uploadEvent) {

        String tipoDoArquivo = uploadEvent.getFile().getContentType();
        String nomeDoArquivo = String.valueOf(System.currentTimeMillis());

        if (tipoDoArquivo.equals("application/msword")) {
            nomeDoArquivo = nomeDoArquivo + ".doc";
        } else if (tipoDoArquivo.equals("application/pdf")) {
            nomeDoArquivo = nomeDoArquivo + ".pdf";
        } else {
            nomeDoArquivo = nomeDoArquivo + ".docx";
        }

        String caminhoDoArquivo = DIR_DOCUMENTOS + nomeDoArquivo;
        InputStream inputStream = null;

        try {
            inputStream = uploadEvent.getFile().getInputstream();
            OutputStream outputStream = new FileOutputStream(new File(caminhoDoArquivo));
            int byteLido;
            byte[] buffer = new byte[2048];
            while ((byteLido = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, byteLido);
            }
            inputStream.close();
            outputStream.flush();
            outputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Anexo novoAnexo = new Anexo();
        novoAnexo.setNome(nomeDoArquivo);
        novoAnexo.setCaminho(caminhoDoArquivo);
        listaDeAnexos.add(novoAnexo);
        dataModelAnexos = new ListDataModel<>(listaDeAnexos);

    }

    public void removeArquivo() {
        Anexo anexo = dataModelAnexos.getRowData();
        File file = new File(anexo.getCaminho());
        if (file.exists()) {
            file.delete();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "Sucesso!",
                    "O arquivo foi removido."));

        }

        listaDeAnexos.remove(anexo);
        dataModelAnexos = new ListDataModel<>(listaDeAnexos);

    }

    @PostConstruct
    public void onConstruct() {
        processoDAO = new ProcessoImpl(manager, transaction);
        encaminhaDAO = new EncaminhamentoImpl(manager, transaction);

    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        processoDAO = null;
        encaminhaDAO = null;
    }

}
