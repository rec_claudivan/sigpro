/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.EncaminhamentoImpl;
import com.sigpro.dao.interfaces.EncaminhaDao;
import com.sigpro.entidades.Encaminhamento;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

/**
 * Created on : 03/12/2014, 11:39:48
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "saidaMB")
@RequestScoped
public class SaidaController {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    @Resource
    private UserTransaction transaction;
    private EncaminhaDao dao;
    private Encaminhamento encaminhamento = new Encaminhamento();
    private DataModel<Encaminhamento> dataModelSaida;

    public DataModel<Encaminhamento> getDataModelSaida() {
        List<Encaminhamento> listaSaida = dao.findAllSaida();
        dataModelSaida = new ListDataModel<>(listaSaida);
        System.out.println("data model: " + dataModelSaida.toString());
        return dataModelSaida;
    }

    public void setDataModelSaida(DataModel<Encaminhamento> dataModelSaida) {
        this.dataModelSaida = dataModelSaida;
    }

    @PostConstruct
    public void onConstruct() {
        dao = new EncaminhamentoImpl(manager, transaction);
    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        dao = null;
        System.out.println("onDestroy()");
    }

}
