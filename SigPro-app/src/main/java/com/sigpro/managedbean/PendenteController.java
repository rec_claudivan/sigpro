/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.managedbean;

import com.sigpro.dao.impl.EncaminhamentoImpl;
import com.sigpro.dao.impl.ProcessoImpl;
import com.sigpro.dao.interfaces.EncaminhaDao;
import com.sigpro.dao.interfaces.ProcessoDao;
import com.sigpro.entidades.Encaminhamento;
import com.sigpro.entidades.Processo;
import com.sigpro.entidades.TipoStatusEncaminhamento;
import com.sigpro.entidades.TipoStatusProcesso;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

/**
 * Created on : 03/12/2014, 15:42:08
 *
 * @author Elis Oliveira
 */
@ManagedBean(name = "pendenteMB")
@RequestScoped
public class PendenteController {

    @PersistenceContext(name = "SIGPRO-PU")
    private EntityManager manager;
    @Resource
    private UserTransaction transaction;
    private EncaminhaDao encaminhaDAO;
    private ProcessoDao processoDAO;
    private Encaminhamento encaminhamento = new Encaminhamento();
    private DataModel<Encaminhamento> dataModelPendente;

    public DataModel<Encaminhamento> getDataModelPendente() {

        if (dataModelPendente == null) {
            List<Encaminhamento> listaPendente = encaminhaDAO.findAllPendente();
            dataModelPendente = new ListDataModel<>(listaPendente);
            System.out.println("data model: " + dataModelPendente.toString());
        }

        return dataModelPendente;

    }

    public void setDataModelPendente(DataModel<Encaminhamento> dataModelPendente) {
        this.dataModelPendente = dataModelPendente;
    }

    public void encaminhaProcesso() {
        //metodo para encaminhar processo para outra unidade
        //a pagina de encaminhamento ainda nao foi feita
        System.out.println("botao de encaminhar processo ainda não esta pronto");
    }

    public void arquivaProcesso() {

        try {

            Encaminhamento e = dataModelPendente.getRowData();

            Processo p = e.getProcesso();
            
            p.setStatusProcesso(TipoStatusProcesso.ARQUIVADO);
            e.setStatus(TipoStatusEncaminhamento.ARQUIVADO);

            processoDAO.update(p);
            System.out.println("atualizou processo");
            encaminhaDAO.update(e);
            System.out.println("atualizou encaminhamento");


            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "Sucesso!",
                    "O processo foi arquivado com sucesso!"));

            dataModelPendente = null;

        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Erro ao tentar arquivar!",
                    "Detalhes do Erro: " + e.getMessage()));
        }

    }

    @PostConstruct
    public void onConstruct() {
        encaminhaDAO = new EncaminhamentoImpl(manager, transaction);
        processoDAO = new ProcessoImpl(manager, transaction);
    }

    @PreDestroy
    public void onDestroy() {
        manager = null;
        encaminhaDAO = null;
        processoDAO=null;
    }

}
