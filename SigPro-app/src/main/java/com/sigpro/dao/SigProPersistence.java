/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao;

import com.sigpro.dao.impl.EncaminhamentoImpl;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Created on : 03/01/2015, 19:46:35
 *
 * @author Elis Oliveira
 */
public class SigProPersistence {

    private EntityManager manager;
    private UserTransaction transaction;

    public SigProPersistence(EntityManager manager, UserTransaction transaction) {
        this.manager = manager;
        this.transaction = transaction;
    }

    public void salva(Object object) {
        try {
            transaction.begin();
            manager.persist(object);
            transaction.commit();
            System.out.println("SALVOU!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Object busca(Class classe, Long id) {
        return manager.find(classe, id);
    }

    public void atualiza(Object object) {
        try {
            transaction.begin();
            manager.merge(object);
            transaction.commit();
            System.out.println("ATUALIZOU!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleta(Object object) {
        try {
            transaction.begin();
            manager.remove(object);
            transaction.commit();
            System.out.println("DELETOU");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Object buscaObjetoComNamedQuery(String namedQuery, Map<String, Object> parametros) {
        Query query = manager.createNamedQuery(namedQuery);
        if (parametros != null) {
            for (String nomeParametro : parametros.keySet()) {
                query.setParameter(nomeParametro, parametros.get(nomeParametro));
            }
        }
        return query.getSingleResult();
    }

    public List buscaListaComNamedQuery(String namedQuery, Map<String, Object> parametros) {
        Query query = manager.createNamedQuery(namedQuery);
        if (parametros != null) {
            for (String nomeParametro : parametros.keySet()) {
                query.setParameter(nomeParametro, parametros.get(nomeParametro));
            }
        }
        return query.getResultList();
    }

    public Object buscaObjetoComJPQLQuery(String jpqlQuery, Map<String, Object> parametros) {
        Query query = manager.createQuery(jpqlQuery);
        if (parametros != null) {
            for (String nomeParametro : parametros.keySet()) {
                query.setParameter(nomeParametro, parametros.get(nomeParametro));
            }
        }
        return query.getSingleResult();
    }

    public List buscaListaComJPQLQuery(String jpqlQuery, Map<String, Object> parametros) {
        Query query = manager.createQuery(jpqlQuery);
        if (parametros != null) {
            for (String nomeParametro : parametros.keySet()) {
                query.setParameter(nomeParametro, parametros.get(nomeParametro));
            }
        }
        return query.getResultList();
    }
}
