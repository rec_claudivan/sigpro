/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sigpro.dao.interfaces;

import com.sigpro.entidades.Unidade;
import java.util.List;

/**
 * Created on : 06/01/2015, 11:58:27
 * @author Elis Oliveira
 */
public interface UnidadeDao {

    public List<Unidade> listarTodasUnidades();
    public Unidade buscaPorNome(String nome);
    
}
