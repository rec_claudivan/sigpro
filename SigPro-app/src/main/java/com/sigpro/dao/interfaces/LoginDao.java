/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sigpro.dao.interfaces;

import com.sigpro.entidades.Usuario;

/**
 * Created on : 14/12/2014, 23:39:27
 * @author Elis Oliveira
 */
public interface LoginDao {

     public Usuario find(String login, String senha);
    
}
