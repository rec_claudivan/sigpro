/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.interfaces;

import com.sigpro.entidades.Encaminhamento;
import java.util.List;

/**
 * Created on : 02/12/2014, 14:40:42
 *
 * @author Elis Oliveira
 */
public interface EncaminhaDao {

    public void save(Encaminhamento encaminhamento);

    public Encaminhamento find(Long id);

    public void update(Encaminhamento encaminhamento);

    public void delete(Encaminhamento encaminhamento);

    public List<Encaminhamento> findAll();

    public List<Encaminhamento> findAllEntrada();

    public List<Encaminhamento> findAllSaida();

    public List<Encaminhamento> findAllPendente();
}
