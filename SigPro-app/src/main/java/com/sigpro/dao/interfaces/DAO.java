/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Created on : 02/12/2014, 09:14:03
 *
 * @author Elis Oliveira
 */
public interface DAO {

    public void persist(Object entity);

    public void update(Object entity);

    public void remove(Object entity);

    public Object find(Long id);

    public List getAll();
    
    public Object getSingleNamedQuery(String namedQuery, Map<String, Object> parametros);

}
