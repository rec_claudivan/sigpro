/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.interfaces;

import com.sigpro.entidades.Processo;
import java.util.List;

/**
 * Created on : 02/12/2014, 12:17:53
 *
 * @author Elis Oliveira
 */
public interface ProcessoDao {

    public void save(Processo processo);

    public Processo find(Long id);

    public void update(Processo processo);

    public void delete(Processo processo);

    public List<Processo> findAll();

    public List<Processo> findAllAtivo();

    public List<Processo> findAllArquivado();

    public List<Processo> filterProcess(String namedQuery, Object key);

    public Processo buscaProcessoPeloCodigo(String namedQuery, Object key);

}
