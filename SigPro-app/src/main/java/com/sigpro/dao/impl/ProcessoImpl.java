/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.impl;

import com.sigpro.entidades.Processo;
import com.sigpro.dao.interfaces.ProcessoDao;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Created on : 02/12/2014, 11:52:46
 *
 * @author Elis Oliveira
 */
public class ProcessoImpl implements ProcessoDao {

    private EntityManager manager;
    private UserTransaction transaction;

    public ProcessoImpl(EntityManager manager, UserTransaction transaction) {
        this.manager = manager;
        this.transaction = transaction;
    }

    public ProcessoImpl() {

    }

    @Override
    public void save(Processo processo) {
        try {
            transaction.begin();
            manager.persist(processo);
            transaction.commit();
            System.out.println("SALVOU PROCESSO DAO!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Processo find(Long id) {
        return manager.find(Processo.class, id);
    }

    @Override
    public void update(Processo processo) {
        try {
            transaction.begin();
            manager.merge(processo);
            transaction.commit();
            System.out.println("ATUALIZOU PROCESSO DAO!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Processo processo) {
        try {
            transaction.begin();
            manager.remove(processo);
            transaction.commit();
            System.out.println("ATUALIZOU PROCESSO DAO!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Processo> findAll() {
        Query q = manager.createQuery("SELECT p FROM Processo p");
        return q.getResultList();
    }

    @Override
    public List<Processo> findAllAtivo() {
        Query q = manager.createQuery("SELECT p FROM Processo p WHERE p.status='ATIVO'");
        return q.getResultList();
    }

    @Override
    public List<Processo> findAllArquivado() {
        Query q = manager.createQuery("SELECT p FROM Processo p WHERE p.status='ARQUIVADO'");
        return q.getResultList();
    }

    @Override
    public List<Processo> filterProcess(String namedQuery, Object key) {
        Query q = manager.createNamedQuery(namedQuery);
        q.setParameter("parametro", key);

        return q.getResultList();
    }
    
    @Override
    public Processo buscaProcessoPeloCodigo(String namedQuery, Object key) {
        Query q = manager.createNamedQuery(namedQuery);
        q.setParameter("parametro", key);
        return (Processo) q.getSingleResult();
    }

}
