/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.impl;

import com.sigpro.dao.interfaces.DAO;
import com.sigpro.dao.interfaces.LoginDao;
import com.sigpro.entidades.Usuario;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on : 14/12/2014, 23:40:11
 *
 * @author Elis Oliveira
 */
public class LoginImpl implements LoginDao {

    private DAO dao;
    
    @Override
    public Usuario find(String login, String senha) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("login", login);
        map.put("senha", senha);
        Object usuarioEncontrado = dao.getSingleNamedQuery("Usuario.buscaLoginSenha", map);
        if (usuarioEncontrado != null) {
            return (Usuario) usuarioEncontrado;
        } else {
            return null;
        }

    }

}
