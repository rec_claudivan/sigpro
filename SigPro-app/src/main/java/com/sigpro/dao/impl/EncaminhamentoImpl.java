/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.impl;

import com.sigpro.dao.interfaces.EncaminhaDao;
import com.sigpro.entidades.Encaminhamento;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Created on : 02/12/2014, 14:42:54
 *
 * @author Elis Oliveira
 */
public class EncaminhamentoImpl implements EncaminhaDao {

    private EntityManager manager;
    private UserTransaction transaction;

    public EncaminhamentoImpl(EntityManager manager, UserTransaction transaction) {
        this.manager = manager;
        this.transaction = transaction;
    }

    @Override
    public void save(Encaminhamento encaminhamento) {
        try {
            transaction.begin();
            manager.persist(encaminhamento);
            transaction.commit();
            System.out.println("SALVOU encaminhamento DAO!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Encaminhamento find(Long id) {
        return manager.find(Encaminhamento.class, id);
    }

    @Override
    public void update(Encaminhamento encaminhamento) {
        try {
            transaction.begin();
            manager.merge(encaminhamento);
            transaction.commit();
            System.out.println("ATUALIZOU NO DAO!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException | 
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Encaminhamento encaminhamento) {
    }

    @Override
    public List<Encaminhamento> findAll() {
        Query q = manager.createQuery("SELECT e FROM Encaminhamento e ORDER BY e.dataEncaminhamento");
        return q.getResultList();
    }

    @Override
    public List<Encaminhamento> findAllEntrada() {
        Query q = manager.createQuery("SELECT e FROM Encaminhamento e WHERE e.status='ENTRADA' ORDER BY e.dataEncaminhamento");
        System.out.println("pegou a query");
        return q.getResultList();
    }

    @Override
    public List<Encaminhamento> findAllSaida() {
        Query q = manager.createQuery("SELECT e FROM Encaminhamento e WHERE e.status='SAIDA' ORDER BY e.dataEncaminhamento");
        return q.getResultList();
    }

    @Override
    public List<Encaminhamento> findAllPendente() {
        Query q = manager.createQuery("SELECT e FROM Encaminhamento e WHERE e.status='PENDENTE' ORDER BY e.dataEncaminhamento");
//        Query q = manager.createQuery("SELECT e FROM Encaminhamento e WHERE e.status='PENDENTE' AND e.dataencaminhamento=(SELECT MAX(en.dataencaminhamento) FROM encaminhamento en)");
        return q.getResultList();
    }

}
