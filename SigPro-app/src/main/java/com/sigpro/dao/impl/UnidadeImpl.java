/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.impl;

import com.sigpro.dao.interfaces.UnidadeDao;
import com.sigpro.entidades.Processo;
import com.sigpro.entidades.Unidade;
import java.sql.Connection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 * Created on : 06/01/2015, 09:56:51
 *
 * @author Elis Oliveira
 */
public class UnidadeImpl implements UnidadeDao {

    private EntityManager manager;
    private UserTransaction transaction;

    public UnidadeImpl() {
    }

    public UnidadeImpl(EntityManager manager, UserTransaction transaction) {
        this.manager = manager;
        this.transaction = transaction;
    }

    @Override
    public List<Unidade> listarTodasUnidades() {
        Query q = manager.createQuery("SELECT u FROM Unidade u");
        return q.getResultList();
    }

    @Override
    public Unidade buscaPorNome(String nome) {
        Query q = manager.createNamedQuery(Unidade.BUSCAR_UNIDADE_PELO_NOME);
        return (Unidade) q.getSingleResult();
    }

}
