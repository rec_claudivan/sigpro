/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.dao.impl;

import com.sigpro.dao.interfaces.DAO;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Created on : 02/12/2014, 09:13:41
 *
 * @author Elis Oliveira
 */
@Named
public class DAOImpl implements DAO {

    private EntityManager manager;
    private UserTransaction transaction;

    public DAOImpl(EntityManager manager, UserTransaction transaction) {
        this.manager = manager;
        this.transaction = transaction;
    }

    @Override
    public void persist(Object entity) {
        try {
            transaction.begin();
            manager.persist(entity);
            transaction.commit();
            System.out.println("SALVOU OBJETO DAO!!!");
        } catch (SystemException | RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException | NotSupportedException ex) {
            Logger.getLogger(EncaminhamentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Object entity) {
        manager.merge(entity);
    }

    @Override
    public void remove(Object entity) {
        try {
            transaction.begin();
            if (manager.contains(entity)) {
                manager.remove(entity);
            } else {
                Object obj = manager.merge(entity);
                manager.remove(obj);
            }
            transaction.commit();

        } catch (NotSupportedException | SystemException |
                RollbackException | HeuristicMixedException |
                HeuristicRollbackException | SecurityException |
                IllegalStateException ex) {
            Logger.getLogger(DAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Object find(Long id) {
        return null;
    }

    @Override
    public List getAll() {
        
        return null;
    }


    @Override
    public Object getSingleNamedQuery(String namedQuery, Map<String, Object> parametros) {
        Query query = manager.createNamedQuery(namedQuery);
        Object o = null;
        if (parametros != null) {
            for (String nomeParametro : parametros.keySet()) {
                query.setParameter(nomeParametro, parametros.get(nomeParametro));
            }
        }
        try {
            o = query.getSingleResult();
        } catch (NoResultException ex) {

        }

        return o;

    }

}
