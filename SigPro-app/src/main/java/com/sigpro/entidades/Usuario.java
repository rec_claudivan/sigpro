/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Elisiany
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "usuario_type", discriminatorType = DiscriminatorType.STRING)
@NamedQuery(name = "Usuario.buscaLoginSenha", query = "select u from Usuario u where u.login= :login and u.senha= :senha")
public class Usuario implements Serializable {

    @Id
    private String matricula;
    private String nome;
    private String email;
    private String login;
    private String senha;

    @OneToMany(mappedBy = "solicitante", cascade = CascadeType.ALL)
    private List<Processo> processosSolicitados = new ArrayList<>();

    public Usuario() {
    }
    
    public Usuario(String matricula){
        this.matricula = matricula;
    }

    public Usuario(String matricula, String nome, String email, String login,
            String senha, List processos) {
        this.matricula = matricula;
        this.nome = nome;
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.processosSolicitados = processos;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Processo> getProcessosSolicitados() {
        return processosSolicitados;
    }

    public void setProcessosSolicitados(List<Processo> processosSolicitados) {
        this.processosSolicitados = processosSolicitados;
    }

    @Override
    public String toString() {
        return "Usuario{" + "matricula=" + matricula + ", nome=" + nome + ", email=" + email + ", login=" + login + ", senha=" + senha + ", processosSolicitados=" + processosSolicitados + '}';
    }

}
