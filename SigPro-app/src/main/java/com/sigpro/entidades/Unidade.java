/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Elisiany
 */
@Entity
@NamedQueries({
     @NamedQuery(name = Unidade.BUSCAR_UNIDADE_PELO_NOME,
            query = "SELECT u FROM Unidade u ORDER BY u.nome ASC")

})
public class Unidade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    public static final String BUSCAR_UNIDADE_PELO_NOME = "buscar.unidade.pelo.nome";

    @OneToMany(mappedBy = "unidade", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Servidor> servidores = new ArrayList<>();

    public Unidade() {
    }

    public Unidade(Long id) {
        this.id = id;
    }

    public Unidade(Long id, String nome, List servidor) {
        this.id = id;
        this.nome = nome;
        this.servidores = servidor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Servidor> getServidores() {
        return servidores;
    }

    public void setServidores(List<Servidor> servidores) {
        this.servidores = servidores;
    }

    @Override
    public String toString() {
        return nome;
    }

}
