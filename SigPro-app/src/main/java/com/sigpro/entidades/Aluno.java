/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Elisiany
 */
@Entity
@DiscriminatorValue("aluno")
public class Aluno extends Usuario implements Serializable {

    private String curso;

    public Aluno() {

    }

    public Aluno(String curso, String matricula, String nome, String email, String login, String senha, List processos) {
        super(matricula, nome, email, login, senha, processos);
        this.curso = curso;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public String toString() {
        return "Aluno{" + "curso=" + curso + '}';
    }

}
