/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Elisiany
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Encaminhamento.BUSCA_TODOS_PENDENTES_DA_UNIDADE,
            query = "SELECT e FROM Encaminhamento e WHERE e.unidadeDestino.id=2 AND e.status='PENDENTE' ORDER BY e.dataEncaminhamento")

})
public class Encaminhamento implements Serializable {

    public static final String BUSCA_TODOS_PENDENTES_DA_UNIDADE = "Encaminhamento.busca.todos.pendentes.da.unidade";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataEncaminhamento;

    @ManyToOne
    private Processo processo;

    @OneToOne
    private Usuario usuarioQueEncaminha;

    @OneToOne
    private Usuario usuarioQueRecebe;

    @OneToOne
    private Unidade unidadeOrigem;

    @OneToOne
    private Unidade unidadeDestino;

    @Enumerated(EnumType.STRING)
    private TipoStatusEncaminhamento status;

    @Enumerated(EnumType.STRING)
    private TipoDecisao decisao;

    private String assunto;
    private String despacho;

    @OneToMany(mappedBy = "encaminhamento", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Anexo> anexos = new ArrayList<>();

    public Encaminhamento() {
    }

    public Encaminhamento(Long id, Date dataEncaminhamento, Processo processo,
            Usuario usuarioQueEncaminha, Usuario usuarioQueRecebe, Unidade unidadeOrigem,
            Unidade unidadeDestino, TipoStatusEncaminhamento status, TipoDecisao decisao,
            String assunto, String despacho, List anexos) {
        this.id = id;
        this.dataEncaminhamento = dataEncaminhamento;
        this.processo = processo;
        this.usuarioQueEncaminha = usuarioQueEncaminha;
        this.usuarioQueRecebe = usuarioQueRecebe;
        this.unidadeOrigem = unidadeOrigem;
        this.unidadeDestino = unidadeDestino;
        this.status = status;
        this.decisao = decisao;
        this.assunto = assunto;
        this.despacho = despacho;
        this.anexos = anexos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataEncaminhamento() {
        return dataEncaminhamento;
    }

    public void setDataEncaminhamento(Date dataEncaminhamento) {
        this.dataEncaminhamento = dataEncaminhamento;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public Usuario getUsuarioQueEncaminha() {
        return usuarioQueEncaminha;
    }

    public void setUsuarioQueEncaminha(Usuario usuarioQueEncaminha) {
        this.usuarioQueEncaminha = usuarioQueEncaminha;
    }

    public Usuario getUsuarioQueRecebe() {
        return usuarioQueRecebe;
    }

    public void setUsuarioQueRecebe(Usuario usuarioQueRecebe) {
        this.usuarioQueRecebe = usuarioQueRecebe;
    }

    public Unidade getUnidadeOrigem() {
        return unidadeOrigem;
    }

    public void setUnidadeOrigem(Unidade unidadeOrigem) {
        this.unidadeOrigem = unidadeOrigem;
    }

    public Unidade getUnidadeDestino() {
        return unidadeDestino;
    }

    public void setUnidadeDestino(Unidade unidadeDestino) {
        this.unidadeDestino = unidadeDestino;
    }

    public TipoStatusEncaminhamento getStatus() {
        return status;
    }

    public void setStatus(TipoStatusEncaminhamento status) {
        this.status = status;
    }

    public TipoDecisao getDecisao() {
        return decisao;
    }

    public void setDecisao(TipoDecisao decisao) {
        this.decisao = decisao;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getDespacho() {
        return despacho;
    }

    public void setDespacho(String despacho) {
        this.despacho = despacho;
    }

    public List<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<Anexo> anexos) {
        this.anexos = anexos;
    }

    @Override
    public String toString() {
        return "Encaminhamento{" + "id=" + id + ", dataEncaminhamento=" + dataEncaminhamento + ", status=" + status + ", decisao=" + decisao + ", assunto=" + assunto + ", despacho=" + despacho + '}';
    }

}
