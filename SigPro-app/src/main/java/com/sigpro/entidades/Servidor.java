/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Elisiany
 */
@Entity
@DiscriminatorValue("servidor")
public class Servidor extends Usuario implements Serializable {

    private String tipoServidor;
    
    @ManyToOne
    private Unidade unidade;

    public Servidor() {
    
    }

    public Servidor(String tipoServidor, Unidade unidade, String matricula, String nome, String email, String login, String senha, List processos) {
        super(matricula, nome, email, login, senha, processos);
        this.tipoServidor = tipoServidor;
        this.unidade = unidade;
    }

    public String getTipoServidor() {
        return tipoServidor;
    }

    public void setTipoServidor(String tipoServidor) {
        this.tipoServidor = tipoServidor;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    @Override
    public String toString() {
        return "Servidor -> " +  getNome() + ", tipoServidor: " + tipoServidor + ", login: " + getLogin();
    }

}
