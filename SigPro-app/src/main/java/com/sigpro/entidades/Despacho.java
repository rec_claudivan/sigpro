/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author Elisiany
 */
@Entity
public class Despacho implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String parecer;

    @ManyToOne
    private Processo processo;

    @OneToOne
    private Usuario usuarioQueEncaminha;

    @OneToOne
    private Usuario usuarioQueRecebe;

    @OneToOne
    private Unidade unidadeOrigem;

    @OneToOne
    private Unidade unidadeDestino;

    public Despacho() {
    }

    public Despacho(Long id, String parecer, Processo processo, Unidade unidadeOrigem, Unidade unidadeDestino) {
        this.id = id;
        this.parecer = parecer;
        this.processo = processo;
        this.unidadeOrigem = unidadeOrigem;
        this.unidadeDestino = unidadeDestino;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParecer() {
        return parecer;
    }

    public void setParecer(String parecer) {
        this.parecer = parecer;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public Usuario getUsuarioQueEncaminha() {
        return usuarioQueEncaminha;
    }

    public void setUsuarioQueEncaminha(Usuario usuarioQueEncaminha) {
        this.usuarioQueEncaminha = usuarioQueEncaminha;
    }

    public Usuario getUsuarioQueRecebe() {
        return usuarioQueRecebe;
    }

    public void setUsuarioQueRecebe(Usuario usuarioQueRecebe) {
        this.usuarioQueRecebe = usuarioQueRecebe;
    }

    public Unidade getUnidadeOrigem() {
        return unidadeOrigem;
    }

    public void setUnidadeOrigem(Unidade unidadeOrigem) {
        this.unidadeOrigem = unidadeOrigem;
    }

    public Unidade getUnidadeDestino() {
        return unidadeDestino;
    }

    public void setUnidadeDestino(Unidade unidadeDestino) {
        this.unidadeDestino = unidadeDestino;
    }

    @Override
    public String toString() {
        return "Despacho -> " + "id: " + id + ", parecer: " + parecer + ", processo: " + processo + ", unidadeOrigem: " + unidadeOrigem + ", unidadeDestino: " + unidadeDestino;
    }

}
