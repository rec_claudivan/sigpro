/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigpro.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Elisiany
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Processo.BUSCAR_POR_ASSUNTO,
            query = "SELECT p FROM Processo p WHERE UPPER(p.assunto) LIKE :parametro ORDER BY p.dataCadastro"),

    @NamedQuery(name = Processo.BUSCAR_POR_UNIDADE,
            query = "SELECT p FROM Encaminhamento e JOIN e.processo p WHERE UPPER(e.unidadeDestino.nome) LIKE :parametro ORDER BY p.dataCadastro"),

    @NamedQuery(name = Processo.BUSCAR_POR_NOME_INTERESSADO,
            query = "SELECT p FROM Processo p WHERE UPPER(p.solicitante.nome) LIKE :parametro ORDER BY p.dataCadastro"),

    @NamedQuery(name = Processo.BUSCAR_POR_NUM_PROCESSO,
            query = "SELECT p FROM Processo p WHERE p.codigo LIKE :parametro ORDER BY p.dataCadastro"),
    
    @NamedQuery(name = Processo.BUSCAR_POR_UM_PROCESSO_PELO_CODIGO,
            query = "SELECT p FROM Processo p WHERE p.codigo=:parametro")

})
public class Processo implements Serializable {

    public static final String BUSCAR_POR_ASSUNTO = "buscar.por.assunto";
    public static final String BUSCAR_POR_UNIDADE = "buscar.por.unidade";
    public static final String BUSCAR_POR_NOME_INTERESSADO = "buscar.por.nome.interessado";
    public static final String BUSCAR_POR_NUM_PROCESSO = "buscar.por.num.processo";
    public static final String BUSCAR_POR_UM_PROCESSO_PELO_CODIGO = "buscar.por.um.processo.pelo.codigo";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String codigo;

    private String assunto;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Enumerated(EnumType.STRING)
    private TipoStatusEncaminhamento statusEncaminhamento;

    @Enumerated(EnumType.STRING)
    private TipoStatusProcesso statusProcesso;

    @Enumerated(EnumType.STRING)
    private TipoDecisao decisao;  
    
    @ManyToOne
    private Usuario solicitante = new Usuario();

    @OneToOne
    private Usuario usuarioQueEncaminha;

    @OneToOne
    private Usuario usuarioQueRecebe;

    @OneToOne
    private Unidade unidadeOrigem;

    @OneToOne
    private Unidade unidadeDestino;

    @OneToMany(mappedBy = "processo", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Encaminhamento> encaminhamentos = new ArrayList<>();

    @OneToMany(mappedBy = "processo", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Despacho> despachos = new ArrayList<>();

    @OneToMany(mappedBy = "processo", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Anexo> anexos = new ArrayList<>();

    public Processo() {
    }

    public Processo(Long id, String codigo, String assunto, Date dataCadastro, TipoStatusEncaminhamento statusEncaminhamento, TipoStatusProcesso statusProcesso, TipoDecisao decisao, Usuario solicitante, Usuario usuarioQueEncaminha, Usuario usuarioQueRecebe, Unidade unidadeOrigem, Unidade unidadeDestino) {
        this.id = id;
        this.codigo = codigo;
        this.assunto = assunto;
        this.dataCadastro = dataCadastro;
        this.statusEncaminhamento = statusEncaminhamento;
        this.statusProcesso = statusProcesso;
        this.decisao = decisao;
        this.solicitante = solicitante;
        this.usuarioQueEncaminha = usuarioQueEncaminha;
        this.usuarioQueRecebe = usuarioQueRecebe;
        this.unidadeOrigem = unidadeOrigem;
        this.unidadeDestino = unidadeDestino;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public TipoStatusEncaminhamento getStatusEncaminhamento() {
        return statusEncaminhamento;
    }

    public void setStatusEncaminhamento(TipoStatusEncaminhamento statusEncaminhamento) {
        this.statusEncaminhamento = statusEncaminhamento;
    }

    public TipoStatusProcesso getStatusProcesso() {
        return statusProcesso;
    }

    public void setStatusProcesso(TipoStatusProcesso statusProcesso) {
        this.statusProcesso = statusProcesso;
    }

    public TipoDecisao getDecisao() {
        return decisao;
    }

    public void setDecisao(TipoDecisao decisao) {
        this.decisao = decisao;
    }

    public Usuario getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Usuario solicitante) {
        this.solicitante = solicitante;
    }

    public Usuario getUsuarioQueEncaminha() {
        return usuarioQueEncaminha;
    }

    public void setUsuarioQueEncaminha(Usuario usuarioQueEncaminha) {
        this.usuarioQueEncaminha = usuarioQueEncaminha;
    }

    public Usuario getUsuarioQueRecebe() {
        return usuarioQueRecebe;
    }

    public void setUsuarioQueRecebe(Usuario usuarioQueRecebe) {
        this.usuarioQueRecebe = usuarioQueRecebe;
    }

    public Unidade getUnidadeOrigem() {
        return unidadeOrigem;
    }

    public void setUnidadeOrigem(Unidade unidadeOrigem) {
        this.unidadeOrigem = unidadeOrigem;
    }

    public Unidade getUnidadeDestino() {
        return unidadeDestino;
    }

    public void setUnidadeDestino(Unidade unidadeDestino) {
        this.unidadeDestino = unidadeDestino;
    }

    public List<Encaminhamento> getEncaminhamentos() {
        return encaminhamentos;
    }

    public void setEncaminhamentos(List<Encaminhamento> encaminhamentos) {
        this.encaminhamentos = encaminhamentos;
    }

    public List<Despacho> getDespachos() {
        return despachos;
    }

    public void setDespachos(List<Despacho> despachos) {
        this.despachos = despachos;
    }

    public List<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<Anexo> anexos) {
        this.anexos = anexos;
    }

    @Override
    public String toString() {
        return "Processo -> " + "id: " + id + ", assunto: " + assunto + ", dataCadastro: "
                + dataCadastro + ", status: " + statusEncaminhamento
                + ", solicitante: " + solicitante + ", decisão: " + decisao;
    }

}
